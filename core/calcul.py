#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Fonctions de calculs pour la Caisse
"""


def calcul_prix_total_ht(prix, nombre):
    """
    calcul le total d'un article
    """
    return prix * nombre


def calcul_prix_remise_ht(prix, seuil, remise):
    """
    Calcul la remise
    """
    if prix > seuil:
        prix = prix - prix * remise / 100
    return prix


def calcul_prix_remise(prix, remise):
    """
    Calcul la remise
    """
    remise = prix * remise / 100
    return remise


def calcul_prix_ttc(prix, tva):
    """
    Calcul le prix TTC
    """
    val = prix + prix * tva
    return val
