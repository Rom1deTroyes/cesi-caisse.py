## 3.1. Mise en place de notre programme de caisse

- Demander à l’utilisateur de saisir un prix hors taxe (HT) ainsi qu’une quantité
- Calculer et afficher le prix total, sur la base d’une TVA de 20%
- Si le total dépasse les 200€, vous effectuerez une remise de 5%
- Vous prendrez soin de vérifier les saisies utilisateurs :
- un prix et une quantité ne peuvent pas être nul ou négatif.

# 3.2. Gestion de plusieurs produits

Vous modifierez le programme précédent pour permettre la gestion de plusieurs produits.
Il faudra donc demander à l’utilisateur combien de produits différents seront à saisir.

# 4.1. Utilisation de données structurées

Cette fois si ce n’est plus l’utilisateur qui devra saisir le prix HT mais les prix seront stockés dans un dictionnaire Python.
La correspondance référence / produit est la suivante :

|Id|Nom   |Prix|
|--|------|----|
| 1|Banane|4   |
| 2|Pomme |2   |
| 3|Orange|1.5 |
| 4|Poire |3   |

Modifier le programme précédent pour permettre la saisir de l’id plutôt que du prix HT.
Vous traiterez les erreurs de saisies utilisateurs par des exceptions plutôt que par des blocs conditionnels if.
Votre programme devra afficher ce type de résultat : 

|Nom   |Prix|Quantité|Total HT|
|------|----|--------|--------|
|Banane|4   |2       |       8|
|Pomme |2   |1       |       2|
|Poire |3   |5       |      15|

+----------------------------------+---------+------------------------+----------------+
| Nom                |  Prix   |       Quantité         |       Total HT |
+----------------------------------+---------+------------------------+----------------+
| Banane                           | 4       | 2                      | 8 |
| Pomme                            | 2       | 1                      | 2 |
| Poire                            | 3       | 5                      | 15 |
+----------------------------------+---------+------------------------+----------------+
Total HT = 25€
Total TTC = 30€

+----------------------------------+---------+------------------------+----------------+
| Nom                |  Prix   |       Quantité         |       Total HT |
+----------------------------------+---------+------------------------+----------------+
| Banane                           | 4       | 20                     | 80 |
| Pomme                            | 2       | 10                     | 20 |
| Poire                            | 3       | 50                     | 150 |
+----------------------------------+---------+------------------------+----------------+

Sous-Total HT = 230€
Remise 5% = 11.5€
Total HT = 218.5€
Total TTC = 262.5€

 
Vous n’y avez peut être pas penser, mais que se passe-t-il si les prix sont en centimes ?
Si vous appliquer une remise, vous aurez un problème sur le nombre de chiffres après la virgules.
Utilisezla biliothèque math pour effectuer un arrondi au centime le plus proche"

