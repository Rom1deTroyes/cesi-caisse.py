#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
- Demander à l’utilisateur de saisir un prix hors taxe (HT) ainsi qu’une quantité
- Calculer et afficher le prix total, sur la base d’une TVA de 20%
- Si le total dépasse les 200€, vous effectuerez une remise de 5%
- Vous prendrez soin de vérifier les saisies utilisateurs :
- un prix et une quantité ne peuvent pas être nul ou négatif.
"""

import csv

import ui.console as ui

TVA = 20 / 100
SEUIL_REMISE = 200
POURCENTAGE_REMISE = 5.0

# Import du catalogue depuis le fichier CSV
CATALOGUE = {}
with open('catalogue/produits.csv', 'r') as csvfile:
    reader = csv.reader(csvfile, quoting=csv.QUOTE_NONE)
    for row in reader:
        CATALOGUE.update({int(row[0]): (str(row[1]), float(row[2]))})

passage = ui.saisir_commande({}, CATALOGUE)

#print("Commande : ", commande)

ui.imprime_commande(passage, CATALOGUE, TVA, SEUIL_REMISE, POURCENTAGE_REMISE)
