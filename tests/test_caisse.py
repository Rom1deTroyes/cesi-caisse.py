#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import caisse


class TestCaisse(object):
    """
    Tests du programme de Caisse
    """
    def test_calcul_prix_total_ht(self):
        """
        Calcul du prix ht
        #return p * q
        """
        assert caisse.calcul_prix_total_ht(0, 0) == 0
        assert caisse.calcul_prix_total_ht(0, 42) == 0
        assert caisse.calcul_prix_total_ht(42, 0) == 0
        assert caisse.calcul_prix_total_ht(100, 1) == 100
        assert caisse.calcul_prix_total_ht(100, 10) == 1000
        assert caisse.calcul_prix_total_ht(24, 10) == 240
        assert caisse.calcul_prix_total_ht(36, -10) == -360

    def test_calcul_prix_remise_ht(self):
        """
        Calcul la remise
        """
        #if p > seuil:
        #    p = p - p * remise
        #return p
        assert caisse.calcul_prix_remise_ht(0, 200, 5) == 0
        assert caisse.calcul_prix_remise_ht(100, 200, 5) == 100
        assert caisse.calcul_prix_remise_ht(200, 200, 5) == 200
        assert caisse.calcul_prix_remise_ht(201, 200, 5) == 201

    def test_calcul_prix_ttc(self):
        """
        Calcul le prix TTC
        """
        assert caisse.calcul_prix_ttc(0, 20) == 0
        assert caisse.calcul_prix_ttc(100, 20) == 120
        assert caisse.calcul_prix_ttc(200, 20) == 240
        assert caisse.calcul_prix_ttc(300, 20) == 360
