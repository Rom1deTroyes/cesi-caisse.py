#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
User interface for terminals.
Use input() and print()
"""

from core.calcul import *


def saisir(val_type, message):
    """
    Saisie une valeur
    """
    val = input(message)
    return val_type(val)


def saisir_produit(catalogue):
    """
    Saisie d'un produit
    """
    imprime_catalogue(catalogue)
    id_produit = saisir(int, "Produit ? ")
    quantite = saisir(int, "Quantité ? ")
    return (id_produit, quantite)


def saisir_commande(commande, catalogue):
    """
    Saisie d'une commande
    """
    nb_produits = saisir(int, "Nombre de produits ? ")
    for produit in range(nb_produits):
        produit = saisir_produit(catalogue)
        print("Produit saisi : ", produit)

        # k = key - commande = dict - i = value
        i = 0
        # si le dictionnaire 'commande' contient la clé 'k'
        # on récupère la valeur
        k = produit[0]
        if k in commande:
            i = commande[k]
        # on ajoute le nombre de produits commandés
        i += int(produit[1])
        # on met à jour l'objet 'i' pour la clé 'k' dans le dictionnaire 'd'
        commande[k] = i
    return commande


def imprime_catalogue(catalogue):
    """
    Imprime un Catalogue
    """
    for article in catalogue:
        print(article, " : ", catalogue[article][0], "(",
              catalogue[article][1], "€)")


def imprime_commande(commande, catalogue, TVA, SEUIL_REMISE, POURCENTAGE_REMISE):
    """
    Imprime une Commande
    """
    BANNER = '+{:^12s}+{:>12s}+{:>12s}+{:>12s}+'.format(
        "-" * 12, "-" * 12, "-" * 12, "-" * 12)

    print(BANNER)
    print('|{:^12s}|{:^12s}|{:^12s}|{:>12s}|'.format("Nom", "Prix", "Quantité",
                                                     "Total HT"))
    print(BANNER)

    prix_total_ht = 0
    for produit in commande:
        prix_ht = catalogue[produit][1]
        quantite = commande[produit]
        prix_article_ht = calcul_prix_total_ht(prix_ht, quantite)
        prix_total_ht = prix_total_ht + prix_article_ht
        print('|{:<12s}|{:>12.2f}|{:>12d}|{:>12.2f}|'.format(
            catalogue[produit][0], prix_ht, quantite, prix_article_ht))

    # prix_total_ht = prix_total_ht + prix_article_ht
    print(BANNER)

    if prix_total_ht > SEUIL_REMISE:
        remise = calcul_prix_remise(prix_total_ht, POURCENTAGE_REMISE)
        print('{:<20s}{:>12.2f}{:<12s}'.format("Sous-Total HT : ",
                                               prix_total_ht, " Euros"))
        print('{:<8s}{:<4.0f}{:<8s}{:>12.2f}{:<12s}'.format(
            "Remise ", POURCENTAGE_REMISE, "% : ", remise, " Euros"))
        PRIX_FINAL_HT = prix_total_ht - remise
    else:
        PRIX_FINAL_HT = prix_total_ht

    print('{:<20s}{:>12.2f}{:<12s}'.format("Total HT : ", PRIX_FINAL_HT,
                                           " Euros"))
    PRIX_FINAL_TTC = calcul_prix_ttc(PRIX_FINAL_HT, TVA)
    print('{:<20s}{:>12.2f}{:<12s}'.format("Total TTC : ", PRIX_FINAL_TTC,
                                           " Euros"))
